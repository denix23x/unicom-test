(function () {
    'use strict';
    angular.module('store-unicom24',['ui.router'])
    .config(mainConfig).controller('mainController', mainController)

/*==============================================
=            Main app configuration            =
==============================================*/

    mainConfig.$inject = ['$urlRouterProvider', '$locationProvider', '$stateProvider'];
    function mainConfig($urlRouterProvider, $locationProvider, $stateProvider) {
        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode({
          enabled: true,
          requireBase: true
        });

        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyDXPTWV5kNNIoAa05SN4xDrQhFe93KtYrI",
            authDomain: "store-unicom24.firebaseapp.com",
            databaseURL: "https://store-unicom24.firebaseio.com",
            projectId: "store-unicom24",
            storageBucket: "",
            messagingSenderId: "994222456592"
            };

        firebase.initializeApp(config);

        // App States
        var states = [{
            name: 'home', 
            url: '/', 
            views:{
                "content": {
                    templateUrl: 'pages/home.html'
                    }
                },                
            },{
            name: 'bucket', 
            url: '/bucket', 
            views:{
                "content": {
                    templateUrl: 'pages/bucket.html'
                    }
                },                
            }];

        states.forEach(function(state) {
          $stateProvider.state(state);
        });

    }

/*=====  End of Main app configuration  ======*/

/*===========================================
=            Main app controller            =
===========================================*/

    mainController.$inject = ['$state'];
    function mainController($state) {
        var main = this,
            bucket = [];

        main.sortType = 'name';
        main.sortReverse = false; 

        main.click = function () {
            console.log(main.products);
        }

        // Get random index for search placeholder
        main.randomness = (function () {
            var randomValue;
            return function () {
                randomValue === undefined ? randomValue = Math.floor(Math.random() * (main.products.length + 1)) : false;
                return randomValue;
            };
        }());

        // Get database
        firebase.database().ref().once('value').then(function(snapshot) {
            main.products = snapshot.val();
            // Set bought count from bucket
            main.products.forEach(function(value, key) {
                main.bucket.forEach(function(value2, key2) {
                    if (value.index === value2.index) {
                        value.bought = value2.bought;
                    }
                })
            }, main.products = _.reject(main.products, function(num) { 
                return num == null; 
            }, $state.reload()));

        });

        // Add product to bucket array
        main.bucketAdd = function (index, minus) {

            // Collect products
            var bucket = {}, match = 0, empty = 0;

            // Find matches
            main.bucket.forEach(function(value, key) {
                value.index === index ? match = match + 1 : false;
                empty = value.count - value.bought;
            });            

            if (match > 0) {
                var test = _.filter(main.bucket, function(item, key) { 
                    // If bought == 0, minus handler
                    if ((item.index == index) && (item.bought.length != 0)) {
                        if (minus) {
                            item.bought = item.bought - 1;
                            if (item.bought == 0) {
                                main.bucket = _.reject(main.bucket, function(num) { 
                                    return num.bought == 0;  
                                });                                
                            }
                        } else if (empty === 0) {
                            return false;
                        } else {
                            item.bought = item.bought + 1;
                        }
                        // If bought != 0 then set bucket value
                        main.products.forEach(function(value, key) {
                            value.index === item.index ? value.bought = item.bought : false;
                        });
                    }
                });
            } else {
                // If item not match with bucket, push(item)
                bucket = _.mapObject(main.products, function(item) { 
                    if ((item.index == index) && (match == 0)) {
                        item.bought = 1;
                        main.bucket.push(item);
                    }               
                });            
            }

        }

        // Insert bucket array in local storage
        angular.element(window).on('beforeunload', function () {
            // main.bucket = [];
            localStorage.setItem('store-bucket', JSON.stringify(main.bucket));
            console.log('qwe11');
        });

        // Get bucket array from local storage
        if ((localStorage.getItem('store-bucket') === null) || (localStorage.getItem('store-bucket') === 'undefined')) {
            main.bucket = [];
            localStorage.setItem('store-bucket', JSON.stringify(main.bucket));
        } else {
            var bucket = JSON.parse(localStorage.getItem('store-bucket'));
            main.bucket = bucket;
        }

        // Remove item from bucket 
        main.removeProduct = function (index) {
            main.bucket.forEach(function(value, key) {
                value.index == index ? main.bucket.splice(key, 1) : false;
            });
            main.products.forEach(function(value, key) {
                value.index == index ? value.bought = '' : false;
            });
        }

        main.clear = function () {
            main.bucket = [];
            localStorage.setItem('store-bucket', JSON.stringify(main.bucket));
            main.products.forEach(function(value, key) {
                main.products[key].bought = '';
            })                      
        }

        // Update database
        main.update = function () {
            var updates = {}, deletes = {};

            // Collect updates
            main.bucket.forEach(function(value, key) {
                main.products.forEach(function(value2, key2) {
                    if (value.index == value2.index) {
                        var count = value2.count - value.bought;
                        if (count === 0) {
                            // Clear empty items
                            firebase.database().ref('/' + value2.index).remove();
                            main.products.splice(key2, 1);
                        } else {
                            // Set new count values
                            updates['/' + value2.index + '/count'] = count;
                            main.products[key2].count = count;
                        }
                    }
                })
            })

            firebase.database().ref().update(updates);

            // Reset all items bought
            main.products.forEach(function(value, key) {
                main.products[key].bought = '';
            })

            // Clear bucket
            main.bucket = [];
            localStorage.setItem('store-bucket', JSON.stringify(main.bucket));

            // Success alert
            angular.element('#bucket-alert').fadeIn(600).delay(2000).fadeOut(300, function () {
                $state.go('home')
            });                    

        }

    }    

/*=====  End of Main app controller  ======*/

})();