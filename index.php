<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="/">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <!-- My Style Inject -->
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body ng-app="store-unicom24">
    <div class="container" ng-controller="mainController as main">
      <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">Unicom Store</a>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav pl-5">
            <li class="nav-item"><a class="nav-link" ui-sref="home" ui-sref-active="active">Список товаров</a></li>
            <li class="nav-item"><a class="nav-link" ui-sref="bucket" ui-sref-active="active">Корзина <span ng-if="main.bucket.length > 0" class="bucket-count">{{main.bucket.length}}</span></a></li>
            <!-- <li class="nav-item"><a class="nav-link" ng-click="main.click()">Click</a></li> -->
          </ul>
        </div>
      </nav>    
      <div class="jumbotron mb-0">
        <p class="lead mb-0">Это приложение является тестовым заданием. Используемый стек - <code>Bootstrap 4</code>, <code>AngularJS</code>, <code>Firebase</code></p>
        <hr>
        <p class="text-muted mb-0"><small>Товары можно добавлять в корзину, изменять количество (данные корзины сохраняются в локальное хранилище браузера).</small></p>
        <p class="text-muted mb-0"><small>После подтверждения "покупки" новые данные синхронизируются с базой данных.</small></p>
        <p class="text-muted mb-0"><small>Ссылка на репозиторий - <a target="_blank" href="https://bitbucket.org/DAMAGE_x1/unicom-test">https://bitbucket.org/DAMAGE_x1/unicom-test</a></small></p>
      </div>          
      <div class="bg-faded px-3 py-4" ui-view="content">
        <i>Some content will load here!</i>
      </div>
    </div>    
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <!-- AngularJS and UI-router Injecting -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script src="https://unpkg.com/angular-ui-router/release/angular-ui-router.min.js"></script>
    <!-- Underscore Injecting -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <!-- Firebase Injecting -->
    <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
    <!-- My App Inject -->
    <script src="js/app.js"></script>
  </body>
</html>
